var database=require('./db');

var getFilesDataModel=function(data,callback){
	var id=data.userid;
	console.log("id :"+id);
	var query='Select * from fileupload where userid='+id+" ORDER BY fileid desc ";
	console.log(query);
	database.query(query,function(err,rows){
		if(err){
			callback({success:false,result:[]},null);
		}
		else{
			var arraydata=new Array();
			for(var i=0;i<rows.length;i++){
				arraydata.push({
					"file_url":rows[i].fileurl,
					"file_type":"Health Records",
					"id":rows[i].fileid,
					"file_desc":"Health record upload from meditrue"
				});
			}
			callback(null,{success:true,result:arraydata});
		}
	});
}

module.exports={
	getFilesDataModel:getFilesDataModel
}