var express = require('express');
var router = express.Router();
var fileupload=require('.././controllers/fileupload');
var getfile=require('.././controllers/getfiles');
var login=require('.././controllers/login.js');
var usersignup=require('.././controllers/signup.js');
var speech=require('.././controllers/speechControl.js')

var getlogin=require('.././controllers/userapi');
var style=require('.././controllers/style.js');

/* GET home page. */
router.get('/', function(req, res, next) {
  res.render('index', { title: 'Express' });
});

router.post('/api/v1/userlogin',getlogin.loginUser);
router.post('/api/addnewstyle',style.addnewstyle);

router.post('/api/login',login.userLogin);
router.post('/api/signup',usersignup.userSignUp);

router.post('/api/stylephoto',fileupload.uploadStylePhoto);

router.post('/api/getStyles',style.getStyles);

router.get('/api/getStyleImage',style.getStyleImage);

router.post('/api/serachStyle',style.serachStyle);




module.exports = router;
