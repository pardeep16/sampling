//var sys = require('sys')

var exec = require('child_process').exec;

var child;

// executes `pwd`

/*child = exec("tesseract /home/pardeep/upload/file.jpg samp -l eng;cat samp.txt", function (error, stdout, stderr) {

  console.log('stdout: ' + stdout);

  //console.log('stderr: ' + stderr);
	

  if (error !== null) {

    console.log('exec error: ' + error);

  }

});
*/
//var search=require('.././custom/patternsearch.js');
var fs=require('fs');


var performOCR=function(data,callback){
	var name=data.name;
	var location=data.location;
	child = exec(" convert "+location+name+" -quality 100 -resize 2000 -sharpen 0x1.2 "+location+"New-"+name+"; ./textcleaner -g -e normalize -f 30 -o 12 -s 2 "+location+"New-"+name+" "+location+"New-"+name+";tesseract "+location+"New-"+name+" "+location+name+" -l eng -psm 1;rm "+location+"New-"+name+";cat "+location+name+".txt", function (error, stdout, stderr) {

 	 //console.log('stderr: ' + stderr);

 	 if (error) {
 	 	callback(stderr,null);

  		}
  		else{
  			
  			//var readdata=fs.readFileSync(location+name+".txt").toString();
  			var newdata=stdout.replace(/[^a-zA-Z0-9\n\b\t./%]/g,' ');
        callback(null,newdata);
  			/*fs.writeFile(location+name+".txt",newdata,function(err){
  				if(err){
  					console.log(err);
  				}
  				else{
  					console.log("success");
            var data={
              "name":name,
              "location": location
            };
            search.patternSearch(data,function(err,result){
              if(err){
                console.log(err);
              }
              else{
                console.log(result);
              }
            });
  				}
  			});
*/  			
  		}

});


}


var performSpeechToText=function(data,callback){
var name=data.name;
  var location=data.location;
  child=exec("pocketsphinx_continuous -infile "+location+name+" > "+location+name+".txt",function (error, stdout, stderr) {
    if(error){
      callback(stderr,null);
    }
    else{
      var readdata=fs.readFileSync(location+name+".txt").toString();
      callback(null,readdata);
    }
  });
  
  }
  
  module.exports={
    performOCR:performOCR,
    performSpeechToText:performSpeechToText
  }
