
var datamodel=require('.././models/getfilesData');

var getFilesData=function(req,res,next){
	var id=req.query.id;
	var data={
		"userid":id
	};
	console.log(id);
	datamodel.getFilesDataModel(data,function(err,data){
		if(err){
			res.send(err);
		}
		else{
			res.send(data);
		}
	});
}

module.exports={
getFilesData:getFilesData
}
