
var fs=require('fs');
var database=require('.././models/db');


var addnewstyle=function(req,res,next){

	console.log(req.body);
	var styleno=req.body.style;
	var category=req.body.category;
	var brand=req.body.brand;
	var product=req.body.product;
	var status=req.body.status;
	var season=req.body.season;
	var userid=req.query.userid;
	var fabric=req.body.fabric;
	var fabricDetails=req.body.fabricdetail;
	var trimDetails=req.body.trimdetail;
	var date=new Date();

	
	var query="Insert into styles(styleno,brand,product,category,userid,status,season,fabric,fabricdetails,trimdetails,date) values('"+styleno+"'"+","+"'"+brand+"'"+","+"'"+product+"'"+","+"'"+category+"'"+","+userid+","+"'"+status+"'"+","+"'"+season+"'"+","+"'"+fabric+"'"+","+"'"+fabricDetails+"'"+","+"'"+trimDetails+"'"+","+"'"+date+"'"+")";
	console.log(query);
	database.query(query,function(err,rows){
		if(err){
			res.send({"success":false,"message":err});
		}
		else{
			res.send({"success":true,"message":"Style Added Successfully!"});
		}

	});

}

var getStyles=function(req,res){
	var status=req.body.status;
	var userid=req.body.userid;


	var query='Select * from styles where userid='+userid+" and status="+"'"+status+"'"+" ORDER BY styleid desc";

	database.query(query,function(err,rows){
		if(err){
			res.send({"success":false,"message":err});
		}
		else{
			var data=new Array();
			if(rows.length>0){
				for(var i=0;i<rows.length;i++){
					var dd=rows[i].date.split(' ');
					console.log(dd);
					data.push({
						"styleid":rows[i].styleid,
						"styleno":rows[i].styleno,
						"brand":rows[i].brand,
						 "product": rows[i].product,      
						 "category": rows[i].category,     
						 "season":  rows[i].season, 
						 "fabric" : rows[i].fabric,   
						 "trimdetails": rows[i].trimdetails,
						 "fabricdetails": rows[i].fabricdetails,
						 "date":dd[0]+" "+dd[1]+" "+dd[2]+" "+dd[3],
						 "status":rows[i].status
					});
				}
				res.send({"success":true,"data":data});
			}
			else{
				res.send({"success":false,"message":"Not Found!"});
			}
		}
	});
}


var getStyleImage=function(req,res){
	var styleno=req.query.styleno;
	var userid=req.query.userid;
	var query='Select * from stylephotos where styleno='+"'"+styleno+"'"+" and userid="+userid;
	console.log(query);
	database.query(query,function(err,rows){
			if(err){
				res.send({"success":false,"message":"Not found"});
			}
			else{
				if(rows.length>0){
					res.send({"success":true,"data":rows[0].photourl});
				}
				else{
					res.send({"success":false,"message":"Not found"});
				}
			}
	});
}

var serachStyle=function(req,res){
	var styleno=req.body.styleno;


	var query='Select * from styles where styleno='+"'"+styleno+"'";

	database.query(query,function(err,rows){
		if(err){
			res.send({"success":false,"message":err});
		}
		else{
			var data=new Array();
			if(rows.length>0){
				for(var i=0;i<rows.length;i++){
					var dd=rows[i].date.split(' ');
					console.log(dd);
					data.push({
						"styleid":rows[i].styleid,
						"styleno":rows[i].styleno,
						"brand":rows[i].brand,
						 "product": rows[i].product,      
						 "category": rows[i].category,     
						 "season":  rows[i].season, 
						 "fabric" : rows[i].fabric,   
						 "trimdetails": rows[i].trimdetails,
						 "fabricdetails": rows[i].fabricdetails,
						 "date":dd[0]+" "+dd[1]+" "+dd[2]+" "+dd[3],
						 "status":rows[i].status
					});
				}
				res.send({"success":true,"data":data});
			}
			else{
				res.send({"success":false,"message":"Not Found!"});
			}
		}
	});
}

module.exports={
	addnewstyle:addnewstyle,
	getStyles:getStyles,
	getStyleImage:getStyleImage,
	serachStyle:serachStyle
}